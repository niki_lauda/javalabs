package main.java.lab1;

import java.io.File;
import java.io.IOException;
import java.text.BreakIterator;
import java.util.*;
import java.util.Map.Entry;

//created by yevhenii maliavka
//on 05.10.16.
//group kp-32
//Target: Count the frequency each word from
//the array occurs in the text.
//Details: count only one word, if occurs, per sentence
public class SentenceCounter {
    private static final int PARAMETERS_COUNT = 2;

    //arguments: textfile location
    //outputs: unique words
    public static void main(String[] args){
        if(args.length != PARAMETERS_COUNT){
            System.out.println("Error:The number of arguments doesn't match.\nExpected: <string> <string>.");
            return;
        }

        File file = new File(args[0]);

        if(!file.exists() || file.isDirectory()){
            System.out.println("Error: file name is invalid or file is not found.");
            return;
        }

        String[] keywords = args[1].split("\\W+");

        try{
            // Construction \Z means the end of the input but for the final terminator, if any
            //http://docs.oracle.com/javase/6/docs/api/java/util/regex/Pattern.html#lt
            //next() returns the next complete token from the scanner, till the delimiter

            //Algorithm
            //1st - get the whole text into single string
            String text = new Scanner(file).useDelimiter("\\Z").next();
            //2nd - parse that string into the list of words collections
            //each word collection represents raw words of the sentence
            List<List<String>> wordsSentences = getWordsSentences(text);
            //3rd - check every word from the list of keywords if they occur
            //in each of the parsed centenses
            //if the list[0](parsed list of words of the first sentence) contains
            //a keyword, then increase count and index to select next collection
            HashMap<String, Integer> wordsMap = getWordsMap(wordsSentences, keywords);
            //4th - print results to console
            printHashMap(wordsMap);
        }
        catch(IOException e){
            System.out.println("Exception Error: " + e.getMessage());
        }
    }

    //arguments: the set of words with their frequency
    //outputs: key-value pair
    private static void printHashMap(HashMap words){
        for (Map.Entry<String,Integer> entry : (Set<Entry>)words.entrySet()) {
            String key = entry.getKey();
            Integer value = entry.getValue();
            System.out.println(key + ": " + value);
        }
    }

    //argument: a full text to parse
    //returns: a list of words collections
    //each words collection represents a parsed sentence;
    private static  List<List<String>> getWordsSentences(String text){
        BreakIterator border = BreakIterator.getSentenceInstance(Locale.US);
        border.setText(text);
        List<String> sentences = new ArrayList<>();

        for (int start = border.first(), end = border.next(); end != BreakIterator.DONE; start = end, end = border.next()) {
            sentences.add(text.substring(start,end));
        }

        List<List<String>> words = new ArrayList<>();

        for(int i = 0; i < sentences.size(); i++){
            words.add(Arrays.asList(sentences.get(i).split("\\W+")));
        }

        return words;
    }

    //arguments: list of word collections, words to count
    //returns: hashmap of <Word>:<Frequency>
    private static HashMap<String, Integer> getWordsMap(List<List<String>> wordsSentences, String[] keywords){
        HashMap<String, Integer> wordsMap = new HashMap<>();

        for(String word : keywords){
            wordsMap.put(word, countFrequency(word, wordsSentences));
        }
        return wordsMap;
    }

    //counts a frequency of a word in each sentence of text
    private static int countFrequency(String word, List<List<String>> wordsSentences){
        int count = 0;
        for(List<String> wordsSentence : wordsSentences){
            if(wordsSentence.contains(word)){
                count++;
            }
        }
        return count;
    }
}
