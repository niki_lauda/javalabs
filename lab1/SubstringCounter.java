package main.java.lab1;

//created by yevhenii maliavka
//on 05.10.16.
//group kp-32
//Target: count occurrences of substring in a string
//Details: Count without overlapping with previously counted
public class SubstringCounter {

    private static final int PARAMETERS_COUNT = 2;

    //1st argument: string
    //2nd argument: substring
    //output: number of occurrences
    public static void main(String[] args) {
        if(args.length != PARAMETERS_COUNT){
            System.out.println("Error:The number of arguments doesn't match.\nExpected: <string>, <string>");
            return;
        }

        String mainString = args[0];
        String substring = args[1];

        if(mainString.length() < substring.length()){
            System.out.println("Error: The main substring cannot be shorter than the source string.");
            return;
        }

        System.out.println("Occurrences: " + countSubstring(mainString, substring));
    }

    private static int countSubstring(String sourceString, String substring){
        for(int count = 0, index = 0; ; count++, index += substring.length()){
            index = sourceString.indexOf(substring, index);
            if(index == -1)
                return count;
        }
    }
}
