package main.java.lab1;

//created by yevhenii maliavka
//on 05.10.16.
//group kp-32
//Target: display all unique words from text
//Details: assume the minimal word length is 1
//Only set of alphanumeric characters is allowed

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Set;

public class TextAnalyser {
    private static final int PARAMETERS_COUNT = 1;
    private static final int MINIMAL_WORD_LENGTH = 2;

    //1st argument: textfile location
    //output: unique words
    public static void main(String[] args){
        if(args.length != PARAMETERS_COUNT){
            System.out.println("Error:The number of arguments doesn't match.\nExpected: <string>.");
            return;
        }

        File file = new File(args[0]);

        if(!file.exists() || file.isDirectory()){
            System.out.println("Error: file name is invalid or file is not found.");
            return;
        }

        try{
            // Construction \Z means the end of the input but for the final terminator, if any
            //http://docs.oracle.com/javase/6/docs/api/java/util/regex/Pattern.html#lt
            //next() returns the next complete token from the scanner, till the delimiter
            String text = new Scanner(file).useDelimiter("\\Z").next();
            String[] words = getUniqueWords(text);
            Arrays.sort(words);
            displayWords(words);
        }
        catch(IOException e){
            System.out.println("Exception Error: " + e.getMessage());
        }
    }

    private static String[] getUniqueWords(String text) {
        //regex \W+ means one or more not alphanumeric characters
        String[] allWords = text.split("\\W+");
        HashMap<String, Integer> wordsMap = new HashMap<>();

        for (String word : allWords) {
            if (wordsMap.containsKey(word)) {
                wordsMap.put(word, wordsMap.get(word) + 1);
            }
            else if(word.length() >= MINIMAL_WORD_LENGTH)
                wordsMap.put(word, 1);
        }

        Set<String> keys = wordsMap.keySet();

        return keys.toArray(new String[keys.size()]);
    }

    private static void displayWords(String[] words){
        for(String word : words){
            System.out.print(word);
            System.out.print(',');
        }
    }
}
